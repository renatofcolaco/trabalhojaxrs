package br.com.cursorws.business;

public class IntegridadeReferencialException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2579968189129905783L;

	public IntegridadeReferencialException (){
		super("Viola��o de integridade referencial.");
	}

	public IntegridadeReferencialException (String message){
		super(message);
	}
}
