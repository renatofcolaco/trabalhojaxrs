package br.com.cursorws.business;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import br.com.cursorws.dao.Repositorio;
import br.com.cursorws.model.Veiculo;

@ApplicationScoped
public class VeiculoBC {

	@Inject
	private Repositorio repositorio;
	
	@PostConstruct
	public void inicializar(){
		
		Calendar data = Calendar.getInstance();
		
		Veiculo veiculo = new Veiculo();
		veiculo.setPlaca("OHX3747");
		veiculo.setNomeProprietario("Renato");
		veiculo.setDataEmplacamento(data.getTime());
		veiculo.setIpva(new BigDecimal("500.00"));
		repositorio.inserir(veiculo);
		
	}
	
	public List<Veiculo> selecionar(){
		return repositorio.selecionar(Veiculo.class);
	}
	
	public Veiculo selecionar(Long id) throws EntidadeNaoEncontradaException
	{
		Veiculo veiculo = repositorio.selecionar(Veiculo.class, id);
		if(veiculo == null){
			throw new EntidadeNaoEncontradaException();
		}
		return veiculo;
	}

	public Long inserir(Veiculo veiculo) throws ValidacaoException {
		validar(veiculo);
		return repositorio.inserir(veiculo);
	}
	
	public void atualizar(Veiculo veiculo) throws ValidacaoException {
		validar(veiculo);
		if (!repositorio.atualizar(veiculo)) {
			throw new ValidacaoException();
		}
	}

	public Veiculo excluir(Long id) throws EntidadeNaoEncontradaException {
		Veiculo veiculo = repositorio.excluir(Veiculo.class, id);
		if (veiculo == null) {
			throw new EntidadeNaoEncontradaException();
		}
		return veiculo;
	}

	private void validar(Veiculo veiculo) throws ValidacaoException {
		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		Set<ConstraintViolation<Veiculo>> violations = validator.validate(veiculo);
		if (!violations.isEmpty()) {
			ValidacaoException validacaoException = new ValidacaoException();
			for (ConstraintViolation<Veiculo> violation : violations) {
				String entidade = violation.getRootBeanClass().getSimpleName();
				String propriedade = violation.getPropertyPath().toString();
				String mensagem = violation.getMessage();
				validacaoException.adicionar(entidade, propriedade, mensagem);
			}
			throw validacaoException;
		}
	}
}