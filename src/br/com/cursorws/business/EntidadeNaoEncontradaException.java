package br.com.cursorws.business;

public class EntidadeNaoEncontradaException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6047675252383897458L;

	public EntidadeNaoEncontradaException (){
		super("Entidade n�o encontrada.");
	}

	public EntidadeNaoEncontradaException (String message){
		super(message);
	}
	
}
