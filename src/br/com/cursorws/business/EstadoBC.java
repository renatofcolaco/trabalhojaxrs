package br.com.cursorws.business;

import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import br.com.cursorws.dao.Repositorio;
import br.com.cursorws.model.Cidade;
import br.com.cursorws.model.Estado;

@ApplicationScoped
public class EstadoBC {

	@Inject
	private Repositorio repositorio;
	
	@PostConstruct
	public void inicializar(){
		
		Estado estado;
		
		estado = new Estado();
		estado.setNome("Cear�");
		estado.setSigla("CE");		
		repositorio.inserir(estado);
		
		estado = new Estado();
		estado.setNome("Maranh�o");
		estado.setSigla("MA");		
		repositorio.inserir(estado);
		
	}
	
	public List<Estado> selecionar(){
		return repositorio.selecionar(Estado.class);
	}
	
	public Estado selecionar(Long id) throws EntidadeNaoEncontradaException
	{
		Estado estado = repositorio.selecionar(Estado.class, id);
		if(estado == null){
			throw new EntidadeNaoEncontradaException();
		}
		return estado;
	}

	public Long inserir(Estado estado) throws ValidacaoException {
		validar(estado);
		return repositorio.inserir(estado);
	}
	
	public void atualizar(Estado estado) throws ValidacaoException {
		validar(estado);
		if (!repositorio.atualizar(estado)) {
			throw new ValidacaoException();
		}
	}

	public Estado excluir(Long id) throws EntidadeNaoEncontradaException, IntegridadeReferencialException {
		
		Estado estado = repositorio.selecionar(Estado.class, id);
		
		List<Cidade> cidades = repositorio.selecionar(Cidade.class);
		
		for (Cidade cidade : cidades) {
			if(cidade.getIdEstado().equals(id)){
				throw new IntegridadeReferencialException("Este estado n�o pode ser exclu�do enquanto possuir cidades cadastradas");
			}			
		}
		
		estado = repositorio.excluir(Estado.class, id);
		if (estado == null) {
			throw new EntidadeNaoEncontradaException();
		}
		
		return estado;
	}

	private void validar(Estado estado) throws ValidacaoException {
		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		Set<ConstraintViolation<Estado>> violations = validator.validate(estado);
		if (!violations.isEmpty()) {
			ValidacaoException validacaoException = new ValidacaoException();
			for (ConstraintViolation<Estado> violation : violations) {
				String entidade = violation.getRootBeanClass().getSimpleName();
				String propriedade = violation.getPropertyPath().toString();
				String mensagem = violation.getMessage();
				validacaoException.adicionar(entidade, propriedade, mensagem);
			}
			throw validacaoException;
		}
	}
}
