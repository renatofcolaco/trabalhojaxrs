package br.com.cursorws.business;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import br.com.cursorws.dao.Repositorio;
import br.com.cursorws.model.Cidade;
import br.com.cursorws.model.Estado;


@ApplicationScoped
public class CidadeBC {

	@Inject
	private Repositorio repositorio;
	
	@PostConstruct
	public void inicializar(){
		Cidade cidade;
		Calendar data = Calendar.getInstance();
		
		cidade = new Cidade();
		cidade.setIdEstado(1L);
		cidade.setNome("Fortaleza");
		cidade.setDataConstituicao(data.getTime());
		cidade.setPib(new BigDecimal("49745920.50"));
		cidade.setPopulacao(862750L);
		repositorio.inserir(cidade);
		
		cidade = new Cidade();
		cidade.setIdEstado(2L);
		cidade.setNome("S�o Lu�s");
		cidade.setDataConstituicao(data.getTime());
		cidade.setPib(new BigDecimal("23132344.50"));
		cidade.setPopulacao(1073893L);
		repositorio.inserir(cidade);
	}
	
	public List<Cidade> selecionarPorEstado(Long idEstado){
		List<Cidade> cidades = repositorio.selecionar(Cidade.class);
		
		List<Cidade> cidadesPorEstado = new ArrayList<Cidade>();
		
		for (Cidade cidade : cidades) {
			
			if(cidade.getIdEstado().equals(idEstado)){
				cidadesPorEstado.add(cidade);
			}
		}
		
		return cidadesPorEstado;
	}
	
	public Cidade selecionar(Long id) throws EntidadeNaoEncontradaException
	{
		Cidade cidade = repositorio.selecionar(Cidade.class, id);
		if(cidade == null){
			throw new EntidadeNaoEncontradaException();
		}
		return cidade;
	}

	public Long inserir(Cidade cidade) throws ValidacaoException, EntidadeNaoEncontradaException {
		validar(cidade);
		
		Estado estado = repositorio.selecionar(Estado.class, cidade.getIdEstado());
		if(estado == null){
			throw new EntidadeNaoEncontradaException(String.format("O estado informado n�o existe. ID: %d",
					cidade.getIdEstado()));
		}
		
		return repositorio.inserir(cidade);
	}
	
	public void atualizar(Cidade cidade) throws ValidacaoException {
		validar(cidade);
		if (!repositorio.atualizar(cidade)) {
			throw new ValidacaoException();
		}
	}

	public Cidade excluir(Long id) throws EntidadeNaoEncontradaException {
				
		Cidade cidade = repositorio.excluir(Cidade.class, id);
		if (cidade == null) {
			throw new EntidadeNaoEncontradaException();
		}
		return cidade;
	}

	private void validar(Cidade cidade) throws ValidacaoException {
		Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
		Set<ConstraintViolation<Cidade>> violations = validator.validate(cidade);
		if (!violations.isEmpty()) {
			ValidacaoException validacaoException = new ValidacaoException();
			for (ConstraintViolation<Cidade> violation : violations) {
				String entidade = violation.getRootBeanClass().getSimpleName();
				String propriedade = violation.getPropertyPath().toString();
				String mensagem = violation.getMessage();
				validacaoException.adicionar(entidade, propriedade, mensagem);
			}
			throw validacaoException;
		}
	}
}
