package br.com.cursorws.model;

import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Estado extends BaseModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3169392460770548749L;

	@Size(min = 3, max = 25)
	@NotNull
	private String nome;
	
	@Size(min = 2, max = 5)
	@NotNull
	private String sigla;
	
	public Estado(){
		super();
	}
	
	public Estado(Long id){
		this();
		setId(id);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
}
