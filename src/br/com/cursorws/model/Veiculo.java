package br.com.cursorws.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import br.com.cursorws.model.xml.DateAdapter;

public class Veiculo extends BaseModel implements Serializable {

	private static final long serialVersionUID = -3064306490724801147L;
	
	@NotNull
	@Size(min = 7, max = 7)
	private String placa;
	
	@NotNull
	@Size(min = 3, max = 50)
	private String nomeProprietario;
	
	@NotNull
	@Past
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date dataEmplacamento;
	
	@DecimalMin("0.00")
	private BigDecimal ipva;
	
	
	public Veiculo(){
		super();
	}
	
	public Veiculo(Long id){
		this();
		setId(id);
	}
	
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getNomeProprietario() {
		return nomeProprietario;
	}
	public void setNomeProprietario(String nomeProprietario) {
		this.nomeProprietario = nomeProprietario;
	}
	public Date getDataEmplacamento() {
		return dataEmplacamento;
	}
	public void setDataEmplacamento(Date dataEmplacamento) {
		this.dataEmplacamento = dataEmplacamento;
	}
	public BigDecimal getIpva() {
		return ipva;
	}
	public void setIpva(BigDecimal ipva) {
		this.ipva = ipva;
	}
	
}
