package br.com.cursorws.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import br.com.cursorws.model.xml.DateAdapter;

public class Cidade extends BaseModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2953941563349341774L;
	
	@NotNull
	private Long idEstado;
		
	@Size(min = 3, max = 50)
	@NotNull
	private String nome;
	
	@NotNull
	@Past
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date dataConstituicao;
	
	@NotNull
	@Min(1)
	private Long populacao;
	
	@NotNull
	@DecimalMin("0.00")
	private BigDecimal pib;
	
	public Cidade(){
		super();
	}
	
	public Cidade(Long id){
		this();
		setId(id);
	}

	public Long getIdEstado() {
		return idEstado;
	}

	public void setIdEstado(Long idEstado) {
		this.idEstado = idEstado;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataConstituicao() {
		return dataConstituicao;
	}

	public void setDataConstituicao(Date dataConstituicao) {
		this.dataConstituicao = dataConstituicao;
	}

	public Long getPopulacao() {
		return populacao;
	}

	public void setPopulacao(Long populacao) {
		this.populacao = populacao;
	}

	public BigDecimal getPib() {
		return pib;
	}

	public void setPib(BigDecimal pib) {
		this.pib = pib;
	}

}
