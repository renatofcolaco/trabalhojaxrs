$(function(){

    var id = obterParametroDaUrlPorNome('id');
    var idEstado = obterParametroDaUrlPorNome('idEstado');
    
    if (id) {
		CidadesProxy.selecionar(id).done(obterOk).fail(tratarErro);
	}

    idEstado = $("#idEstado").val() ||  idEstado;

	var linkVoltar = document.getElementById("voltar");
	linkVoltar.setAttribute("href","estados-editar.html?id=" + idEstado);

    $("#salvar").click(
        function(event) {

            limparMensagensErro();

            var cidade = {
                id : $("#id").val(),
                idEstado: idEstado,
                nome : $("#nome").val(),
                dataConstituicao : $("#dataConstituicao").val(),
                populacao: $("#populacao").val(),
                pib: $("#pib").val()
            };

            if (cidade.id) {
                CidadesProxy.atualizar(cidade.id, cidade).done(atualizarOk).fail(tratarErro);
            } else {
                CidadesProxy.inserir(cidade).done(inserirOk).fail(tratarErro);
            }

    });

	$("#excluir").click(function(event) {
		var id = $("#id").val();
		CidadesProxy.excluir(id).done(excluirOk).fail(tratarErro);
	});

});

function inserirOk(data, textStatus, jqXHR) {
	$("#id").val(data);
	$("#global-message").addClass("alert-success").text(
			"Cidade com id = " + data + " criado com sucesso.").show();
}

function tratarErro(request) {
	switch (request.status) {
	case 403:
		// utilizei o código 403, mas n sei qual o código correto para problemas de integridade referencial
		$("#global-message").addClass("alert-danger").text(request.responseText).show();
		break;
	case 406:
		$("form input").each(function() {
			var id = $(this).attr("id");
			var message = null;
			$.each(request.responseJSON, function(index, value) {
				if (id == value.propriedade) {
					message = value.mensagem;
				}
			});
			if (message) {
				$("#" + id).parent().addClass("has-error");
				$("#" + id + "-message").html(message).show();
				$(this).focus();
			} else {
				$("#" + id).parent().removeClass("has-error");
				$("#" + id + "-message").hide();
			}
		});
		$("#global-message").addClass("alert-danger").text(
				"Verifique erros no formulário!").show();
		break;
	case 404:
		$("#global-message").addClass("alert-danger").text(
				"O registro solicitado não foi encontrado!").show();
		break;
	default:
		$("#global-message").addClass("alert-danger").text("Erro inesperado.").show();
		break;
	}
}

function obterOk(data) {
	$("#id").val(data.id);
    $("#idEstado").val(data.idEstado);
	$("#nome").val(data.nome);
	$("#dataConstituicao").val(data.dataConstituicao);
	$("#populacao").val(data.populacao);
	$("#pib").val(data.pib);
    
}

function atualizarOk(data, textStatus, jqXHR) {
	$("#global-message").addClass("alert-success").text(
			"Cidade atualizada com sucesso.").show();
}

function excluirOk(data, textStatus, jqXHR) {
	$("#id").val(null);
    $("#idEstado").val(null);
	$("#nome").val(null);
	$("#dataConstituicao").val(null);
	$("#populacao").val(null);
	$("#pib").val(null);
	
	$("#global-message").addClass("alert-success").text(
			"Cidade excluída com sucesso.").show();
}

function limparMensagensErro() { /* Limpa as mensagens de erro */
	$("#global-message").removeClass("alert-danger alert-success").empty()
			.hide();
	$(".control-label").parent().removeClass("has-success");
	$(".text-danger").parent().removeClass("has-error");
	$(".text-danger").hide();
}