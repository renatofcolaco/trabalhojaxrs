$(function() {
	EstadosProxy.selecionarTodos().done(buscarOk);
});

function buscarOk(dados) {
	var body = document.getElementsByTagName("tbody")[0];
	$(body).empty();
	if (dados) {

		var docFragment = document.createDocumentFragment();

		for (var i = 0; i < dados.length; i++) {
			
			var estado = dados[i];
			var row = document.createElement('tr');
			
			var cellID = document.createElement('td');
			var textID = document.createTextNode(estado.id);
			cellID.appendChild(textID);

			var cellNome = document.createElement('td');
			var textNome = document.createTextNode(estado.nome);
			cellNome.appendChild(textNome);

			var cellSigla = document.createElement('td');
			var textSigla = document.createTextNode(estado.sigla);
			cellSigla.appendChild(textSigla);
			
			var cellEditar = document.createElement('td');
			var textEditar = document.createTextNode('Editar');
			var linkEditar = document.createElement('a');
			linkEditar.setAttribute("href", "estados-editar.html?id="	+ estado.id);
			linkEditar.setAttribute("class", "btn btn-default");
			linkEditar.appendChild(textEditar);
			cellEditar.appendChild(linkEditar);

			row.appendChild(cellID);			
			row.appendChild(cellNome);
			row.appendChild(cellSigla);
			row.appendChild(cellEditar);
						
			docFragment.appendChild(row);
			
		}

		body.appendChild(docFragment);

	} else {
		var table = document.getElementsByTagName("table")[0];
		var foot = document.createElement('tfoot');
		var emptyRow = document.createElement('tr');
		var emptyCell = document.createElement('td');
		var noRegisterText = document
				.createTextNode("Nenhum registro encontrado!");
		emptyCell.appendChild(noRegisterText);
		emptyCell.setAttribute("colspan", 4);
		emptyRow.appendChild(emptyCell);
		foot.appendChild(emptyRow);
		table.appendChild(foot);
	}
}