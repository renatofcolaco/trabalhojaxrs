$(function() {
	VeiculosProxy.selecionarTodos().done(buscarOk);
});

function buscarOk(dados) {
	var body = document.getElementsByTagName("tbody")[0];
	$(body).empty();
	if (dados) {

		var docFragment = document.createDocumentFragment();

		for (var i = 0; i < dados.length; i++) {
			
			var veiculo = dados[i];
			var row = document.createElement('tr');
			

			var cellPlaca = document.createElement('td');
			var textPlaca = document.createTextNode(veiculo.placa);
			var linkPlaca = document.createElement('a');
			linkPlaca.setAttribute("href", "veiculos-editar.html?id="	+ veiculo.id);
			linkPlaca.appendChild(textPlaca);
			cellPlaca.appendChild(linkPlaca);

			var cellNomeProprietario = document.createElement('td');
			var textNomeProprietario = document.createTextNode(veiculo.nomeProprietario);
			cellNomeProprietario.appendChild(textNomeProprietario);

			var cellDataEmplacamento = document.createElement('td');
			var textDataEmplacamento = document.createTextNode(veiculo.dataEmplacamento);
			cellDataEmplacamento.appendChild(textDataEmplacamento);

			var cellIpva = document.createElement('td');
			var textNascimento = document.createTextNode(veiculo.ipva);
			cellIpva.appendChild(textNascimento);

			row.appendChild(cellPlaca);
			row.appendChild(cellNomeProprietario);
			row.appendChild(cellDataEmplacamento);
			row.appendChild(cellIpva);
						
			docFragment.appendChild(row);
			
		}

		body.appendChild(docFragment);

	} else {
		var table = document.getElementsByTagName("table")[0];
		var foot = document.createElement('tfoot');
		var emptyRow = document.createElement('tr');
		var emptyCell = document.createElement('td');
		var noRegisterText = document
				.createTextNode("Nenhum registro encontrado!");
		emptyCell.appendChild(noRegisterText);
		emptyCell.setAttribute("colspan", 4);
		emptyRow.appendChild(emptyCell);
		foot.appendChild(emptyRow);
		table.appendChild(foot);
	}
}