$(function(){

    var id = obterParametroDaUrlPorNome('id');
	if (id) {
		EstadosProxy.selecionar(id).done(obterOk).fail(tratarErro);

		setUrlNovaCidade(id);

		selecionarCidadePorEstado(id);
	}

    $("#salvar").click(
        function(event) {

            limparMensagensErro();

            var estado = {
                id : $("#id").val(),
                nome : $("#nome").val(),
                sigla : $("#sigla").val()
            };

            if (estado.id) {
                EstadosProxy.atualizar(estado.id, estado).done(atualizarOk).fail(tratarErro);
            } else {
                EstadosProxy.inserir(estado).done(inserirOk).fail(tratarErro);
            }

    });

	$("#excluir").click(function(event) {
		var id = $("#id").val();
		EstadosProxy.excluir(id).done(excluirOk).fail(tratarErro);
	});

});

function setUrlNovaCidade(id){
	var btnNovaCidade = document.getElementById('novaCidade');
	btnNovaCidade.setAttribute("href", "cidades-editar.html?idEstado="	+ id);
}

function inserirOk(data, textStatus, jqXHR) {
	$("#id").val(data);
	setUrlNovaCidade(data);
	$("#global-message").addClass("alert-success").text(
			"Estado com id = " + data + " criado com sucesso.").show();
}

function tratarErro(request) {
	switch (request.status) {
	case 403:
		// utilizei o código 403, mas n sei qual o código correto para problemas de integridade referencial
		$("#global-message").addClass("alert-danger").text(request.responseText).show();
		break;
	case 406:
		$("form input").each(function() {
			var id = $(this).attr("id");
			var message = null;
			$.each(request.responseJSON, function(index, value) {
				if (id == value.propriedade) {
					message = value.mensagem;
				}
			});
			if (message) {
				$("#" + id).parent().addClass("has-error");
				$("#" + id + "-message").html(message).show();
				$(this).focus();
			} else {
				$("#" + id).parent().removeClass("has-error");
				$("#" + id + "-message").hide();
			}
		});
		$("#global-message").addClass("alert-danger").text(
				"Verifique erros no formulário!").show();
		break;
	case 404:
		$("#global-message").addClass("alert-danger").text(
				"O registro solicitado não foi encontrado!").show();
		break;
	default:
		$("#global-message").addClass("alert-danger").text("Erro inesperado.").show();
		break;
	}
}

function obterOk(data) {
	$("#id").val(data.id);
	$("#nome").val(data.nome);
	$("#sigla").val(data.sigla);
}

function atualizarOk(data, textStatus, jqXHR) {
	$("#global-message").addClass("alert-success").text(
			"Estado atualizado com sucesso.").show();
}

function excluirOk(data, textStatus, jqXHR) {
	$("#id").val(null);
	$("#nome").val(null);
	$("#sigla").val(null);
	
	$("#global-message").addClass("alert-success").text(
			"Estado excluído com sucesso.").show();
}

function limparMensagensErro() { /* Limpa as mensagens de erro */
	$("#global-message").removeClass("alert-danger alert-success").empty()
			.hide();
	$(".control-label").parent().removeClass("has-success");
	$(".text-danger").parent().removeClass("has-error");
	$(".text-danger").hide();
}